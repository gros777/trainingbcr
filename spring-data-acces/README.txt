#####################################################
## Este es un proyecto de caracter educativo       ##
## en el cual se ejemplifica el uso de JUnit y     ##
## su integración con Spring 3.2.2 y jdbcTemplate  ##
#####################################################

Requerimientos: 
->Tomcat 7, se utiliza el plugin de maven para un despliegue facil y se asume que 
  se tiene funcionando un servidor tomcat y que el usuario admin tiene como password 
  admin y que este tiene el rol manager-script asignado
-> Spring Tool Suite
-> Maven 3

NOTA: los comando proporcionados son para utilizar en sistemas tipo *nix

Preparación del ambiente
1. compilar con maven: mvn clean package
2. cargar proyecto en STS (o eclipse) con Import>Existing Maven Project
3. Agregar el servidor tomcat al archivo $M2_HOME/conf/settings.xml dentro de
tag <servers>:
    <server>
      <id>myTomcat</id>
      <username>admin</username>
      <password>admin</password>
    </server>
4. Agregar el siguiente data source en tomcat: $CATALINA_HOME/conf/context.xml
   <Resource 
	name="jdbc/prodRegDataSrc" 
	auth="Container" 
	type="javax.sql.DataSource" 
	username="sa" 
	password="" 
	driverClassName="org.hsqldb.jdbcDriver" 
	url="jdbc:hsqldb:hsql://localhost:9001" 
	maxActive="8" 
	maxIdle="4"/>
5. copiar librería hsql a $CATALINA_HOME/lib de target/producto-register-app/WEB-INF/lib
   con el comando:
   cp target/producto-register-app/WEB-INF/lib/hsqldb-2.2.4.jar $CATALINA_HOME/lib
6. Iniciar servidor hsql desde el raíz del proyecto:
   mvn exec:java -Dexec.mainClass="org.hsqldb.Server" -Dexec.args="-database.0 file:data/tutorial"
7. iniciar servidor tomcat
6. desplegar aplicativo:
   mvn tomcat7:deploy
8. visitar http://localhost:8080/spring-unit-test/

#############################################################
#                Notas para el desarrollo                   #
#############################################################

-. Para el uso de las pruebas unitarias se esta utilizando SpringUnitTest con el archivo de
   configuración spring-test-beans.xml ubicado en resourses/META-INF/spring. Debido al uso de la
   la base de datos en las pruebas este esta configurado con el datasource C3PO.
-. Se han desactivado las pruebas unitarias de forma estandar en maven (con el plugin surefire).
   Para correr las pruebas unitarias se debe usar el siguiente comando maven:
   mvn -DskipTests=false test
-. Si se necesitase realizar un debuggin con eclipse se puede utilizar el comando maven:
   mvn -Dmaven.surefire.debug -DskipTests=false test
   este utiliza el dt_socket=5005 para escuchar las conexiones. El funcionamiento es simple: cuando
   maven llega a la etapa TEST, este pausa la VM y espera una conexión al puerto 5005, una vez
   conectado el debuger a la VM la ejecución de los TEST dará comienzo. Si es necesario utilizar otro
   puerto se debera pasar a maven de la siguiente forma:
   mvn -Dmaven.surefire.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8000 -Xnoagent -Djava.compiler=NONE" -DskipTests=false test

