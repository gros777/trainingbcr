package sv.gob.bcr.springdataacces.dao.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import sv.gob.bcr.springdataacces.dao.UsuarioDao;
import sv.gob.bcr.springdataacces.exception.DaoException;
import sv.gob.bcr.springdataacces.model.Usuario;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/app-test-context.xml")
public class UsuarioDaoTest {

	@Resource(name = "usuarioDaoJdbcTem")
	UsuarioDao usuarioDao;
	private static final String NOMBRE = "Salchichonio";
	private static final String APELLIDO = "Cerdini";
	private static final String NUEVO_NOMBRE = "Morsalini";

	@Test
	public void autowiringTest() {
		Assert.assertNotNull("usuario dao nulo", usuarioDao);
	}

	@Test(expected = UncategorizedSQLException.class)
	public void userDaoTest() throws ParseException, DaoException {
		// Insert
		Usuario nuevoUsuario = new Usuario();
		nuevoUsuario.setNombre(NOMBRE);
		nuevoUsuario.setApellido(APELLIDO);
		nuevoUsuario.setDireccion("Col San Luis, Pje i casa #10, Santa Ana");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		nuevoUsuario.setFechaNacimiento(sdf.parse("04/10/1978"));
		usuarioDao.create(nuevoUsuario);

		// Selects
		Usuario usuario = usuarioDao.getUsuariosByName("Salchichonio").get(0);
		Assert.assertNotNull("El usuario no existe con ese nombre", usuario);
		Assert.assertNotNull("El usuario no existe para este id",
				usuarioDao.getUsuario(usuario.getId()));
		Assert.assertNotNull(
				"El usuario no existe para este nombre y apellido",
				usuarioDao.getUsuario(NOMBRE, APELLIDO));

		// Modificacion
		usuario.setNombre(NUEVO_NOMBRE);
		usuarioDao.update(usuario);
		Usuario usuMod = usuarioDao.getUsuario(NUEVO_NOMBRE, APELLIDO);
		Assert.assertNotNull("Modificacion fallo", usuMod);

		// Ellimininacion
		usuarioDao.delete(usuMod);
		Assert.assertNull("Borrado fallo",
				usuarioDao.getUsuario(NUEVO_NOMBRE, APELLIDO));
	}

}
