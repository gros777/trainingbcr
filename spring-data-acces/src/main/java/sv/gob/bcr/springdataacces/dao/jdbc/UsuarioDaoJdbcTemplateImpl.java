package sv.gob.bcr.springdataacces.dao.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import sv.gob.bcr.springdataacces.dao.UsuarioDao;
import sv.gob.bcr.springdataacces.exception.DaoException;
import sv.gob.bcr.springdataacces.model.Usuario;

@Repository(value = "usuarioDaoJdbcTem")
@Transactional(readOnly=true)
public class UsuarioDaoJdbcTemplateImpl implements UsuarioDao {

	private static final String SELECT_ALL = "select * from Person";
	private static final String INSERT = "insert into Person(FIRSTNAME, LASTNAME, ADDRESS, BIRTHDATE) values(?, ?, ?, ?)";
	private static final String SELECT_ALL_BY_NAME = "select * from Person where FIRSTNAME = ?";
	private static final String SELECT_ONE = "select * from Person where ID = ?";
	private static final String SELECT_BY_FULL_NAME = "select * from Person where FIRSTNAME= ? and LASTNAME = ?";
	private static final String UPDATE = "update Person set FIRSTNAME = ?, LASTNAME = ?, ADDRESS = ?, BIRTHDATE = ? where ID = ?";
	private static final String DELETE = "delete from Person where ID = ?";

	@Autowired
	JdbcTemplate template;

	public JdbcTemplate getTemplate() {
		return template;
	}

	public void setTemplate(JdbcTemplate template) {
		this.template = template;
	}

	@Override
	public List<Usuario> getAllUsuarios() {
		return template.query(SELECT_ALL, new UsuarioMapper());
	}

	@Override
	public List<Usuario> getUsuariosByName(String nombre) throws DaoException {

		return template.query(SELECT_ALL_BY_NAME, new Object[] { nombre },
				new UsuarioMapper());
	}

	@Override
	public Usuario getUsuario(Long id) throws DaoException {
		return template.queryForObject(SELECT_ONE, new Object[] { id },
				new UsuarioMapper());
	}

	@Override
	public Usuario getUsuario(String nombre, String apellido)
			throws DaoException {
		Usuario usuario = null;
		List<Usuario> resultado = this.template.query(SELECT_BY_FULL_NAME, new Object [] { nombre, apellido }, new UsuarioMapper());
		if (!resultado.isEmpty())
			usuario = resultado.get(0);
		return usuario;
	}

	@Override
	public void create(Usuario newUsuario) throws DaoException {
		this.template.update(INSERT,
				new Object[] { newUsuario.getNombre(),
						newUsuario.getApellido(), newUsuario.getDireccion(),
						newUsuario.getFechaNacimiento() });

	}

	@Override
	public void delete(Usuario usuario) throws DaoException {
		this.template.update(DELETE, new Object[] { usuario.getId() });
	}

	@Override
	public void update(Usuario usuario) throws DaoException {

		this.template.update(UPDATE,
				new Object[] { usuario.getNombre(), usuario.getApellido(),
						usuario.getDireccion(), usuario.getFechaNacimiento(),
						usuario.getId() });

	}

	private final class UsuarioMapper implements RowMapper<Usuario> {

		@Override
		public Usuario mapRow(ResultSet rs, int numRow) throws SQLException {
			Usuario usuario = new Usuario();
			usuario.setId(rs.getLong("ID"));
			usuario.setNombre(rs.getString("FIRSTNAME"));
			usuario.setApellido(rs.getString("LASTNAME"));
			usuario.setDireccion(rs.getString("ADDRESS"));
			usuario.setFechaNacimiento(rs.getDate("BIRTHDATE"));
			return usuario;
		}

	}

}
