package sv.gob.bcr.springdataacces.service.impl;

import java.util.Date;

import javax.annotation.Resource;
import javax.management.timer.Timer;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import sv.gob.bcr.springdataacces.dao.UsuarioDao;
import sv.gob.bcr.springdataacces.exception.DaoException;
import sv.gob.bcr.springdataacces.exception.RegistryServiceException;
import sv.gob.bcr.springdataacces.model.Usuario;
import sv.gob.bcr.springdataacces.service.RegistryService;

@Service(value = "registryService")
public class RegistryServiceImpl implements RegistryService {

	@Resource(name = "usuarioDaoJdbcTem")
	UsuarioDao usuarioDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, readOnly = false)
	public void registryNewUser(Usuario usuario)
			throws RegistryServiceException {
		if (usuario == null)
			throw new RegistryServiceException("El usuario recibido es nulo");

		if (usuario.getNombre() == null || usuario.getNombre().equals(""))
			throw new RegistryServiceException(
					"El nombre del usuario es nulo o esta vacio");

		if (usuario.getApellido() == null || usuario.getApellido().equals(""))
			throw new RegistryServiceException(
					"El apellido del usuario es nulo o esta vacio");
		try {
			if (usuarioDao.getUsuario(usuario.getNombre(),
					usuario.getApellido()) != null)
				throw new RegistryServiceException(
						"El usuario ya esta registrado");
		} catch (DaoException e) {
			e.printStackTrace();
			throw new RegistryServiceException("Error en el acceso a datos", e);
		}
		//Validacion de la edad de registro
		Long age =  (new Date().getTime() - usuario.getFechaNacimiento().getTime()) / (365L * Timer.ONE_DAY);
		if ( age < 15L )
			throw new RegistryServiceException(
					"La edad no puede ser menor a 15 anios");
		if ( age > 100L )
			throw new RegistryServiceException("La edad no puede ser mayor a 100 anios");


		try {
			usuarioDao.create(usuario);
		} catch (DaoException e) {
			e.printStackTrace();
			throw new RegistryServiceException("Error creando usuario", e);
		}
	}

	@Override
	public void unregistryUser(Usuario usuario) throws RegistryServiceException {
		// TODO Auto-generated method stub

	}

	@Override
	public void modifyUser(Usuario usuario) throws RegistryServiceException {
		// TODO Auto-generated method stub

	}

}
