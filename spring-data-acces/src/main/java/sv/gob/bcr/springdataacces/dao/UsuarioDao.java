package sv.gob.bcr.springdataacces.dao;

import java.util.List;

import sv.gob.bcr.springdataacces.exception.DaoException;
import sv.gob.bcr.springdataacces.model.Usuario;

public interface UsuarioDao {

	List<Usuario> getAllUsuarios();
	List<Usuario> getUsuariosByName(String nombre) throws DaoException;
	Usuario getUsuario(Long id) throws DaoException;
	Usuario getUsuario(String nombre, String apellido) throws DaoException;
	void create(Usuario newUsuario) throws DaoException;
	void delete(Usuario usuario) throws DaoException;
	void update(Usuario usuario) throws DaoException;

}
