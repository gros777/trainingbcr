package sv.gob.bcr.springdataacces.service;

import sv.gob.bcr.springdataacces.exception.RegistryServiceException;
import sv.gob.bcr.springdataacces.model.Usuario;

public interface RegistryService {
	
	public void registryNewUser(Usuario usuario) throws RegistryServiceException;
	public void unregistryUser(Usuario usuario) throws RegistryServiceException;
	public void modifyUser(Usuario usuario) throws RegistryServiceException;


}
