/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.bo.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sv.gob.mined.apps.bnprove.mvn.bo.UserBo;
import sv.gob.mined.apps.bnprove.mvn.dao.SampleDao;
import sv.gob.mined.apps.bnprove.mvn.fentities.Usuario;

/**
 *
 * @author xtecuan
 */
@Service(value = "userBo")
public class UserBoImpl implements UserBo {

    private static final Logger loggerF = Logger.getLogger(UserBoImpl.class);

    @Autowired
    private SampleDao daoUsuario;

    public void setDaoUsuario(SampleDao daoUsuario) {
        this.daoUsuario = daoUsuario;
    }

    @Transactional(readOnly = true)
    @Override
    public String getMessage() {
        return "JSF 2 + Spring Integration";
    }

    @Transactional(readOnly = true)
    @Override
    public String getMessage(String names, String lastNames) {
        return "Hello User: " + names + " " + lastNames;
    }

    @Transactional(readOnly = false)
    @Override
    public Usuario guardarUsuario(Usuario usuario) {
        return daoUsuario.guardarUsuario(usuario);
    }

}
