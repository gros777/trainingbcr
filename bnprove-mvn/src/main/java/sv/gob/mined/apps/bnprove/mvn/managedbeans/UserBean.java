/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.managedbeans;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sv.gob.mined.apps.bnprove.mvn.bo.UserBo;

/**
 *
 * @author xtecuan
 */
@Component
@ManagedBean
@ViewScoped
public class UserBean implements Serializable {

    @Autowired
    private UserBo userBo;
    private String salida;
    private String names;
    private String lastNames;

    public UserBo getUserBo() {
        return userBo;
    }

    public void setUserBo(UserBo userBo) {
        this.userBo = userBo;
    }

    public String printMsgFromSpring() {
        return userBo.getMessage();
    }

    public String getSalida() {
        return userBo.getMessage();
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastNames() {
        return lastNames;
    }

    public void setLastNames(String lastNames) {
        this.lastNames = lastNames;
    }

    public void saveUser(ActionEvent event) {

        this.salida = userBo.getMessage(names, lastNames);

        agregarMensaje("Mensaje Salida: ", salida);

    }

    private void agregarMensaje(String summary, String detail) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
    }

}
