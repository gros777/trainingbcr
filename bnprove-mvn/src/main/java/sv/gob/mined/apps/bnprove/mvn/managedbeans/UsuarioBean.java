/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.managedbeans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sv.gob.mined.apps.bnprove.mvn.bo.UserBo;
import sv.gob.mined.apps.bnprove.mvn.fentities.Usuario;

/**
 *
 * @author xtecuan
 */
@Component
@ManagedBean
@ViewScoped
public class UsuarioBean implements Serializable {

    @Autowired
    private UserBo userBo;

    private Usuario user;

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public UsuarioBean() {
    }

    @PostConstruct
    private void init() {
        this.user = new Usuario();
    }

    public UserBo getUserBo() {
        return userBo;
    }

    public void setUserBo(UserBo userBo) {
        this.userBo = userBo;
    }

    public String printMsgFromSpring() {
        return userBo.getMessage();
    }

    public String getSalida() {
        return userBo.getMessage();
    }

    public void saveUser(ActionEvent event) {

        Usuario u = userBo.guardarUsuario(user);
        agregarMensaje("Salida:", "Se creo un usuario con id: " + u.getId());
        user = new Usuario();
    }

    private void agregarMensaje(String summary, String detail) {

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary, detail));
    }

}
