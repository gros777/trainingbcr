/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.bo;

import sv.gob.mined.apps.bnprove.mvn.fentities.Usuario;

/**
 *
 * @author xtecuan
 */
public interface UserBo {

    public String getMessage();

    public String getMessage(String names, String lastNames);

    public Usuario guardarUsuario(Usuario usuario);
}
