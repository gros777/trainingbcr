/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.fentities;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author xtecuan
 */
public class Usuario implements Serializable {

    public static final String QUERY_ALL = "select * from USUARIO order by id";
    public static final String QUERY_BY_ROWID = "select * from USUARIO where rowid=?";
    public static final String INSERT = "INSERT INTO USUARIO (NOMBRES, APELLIDOS, FECHANAC) VALUES (?, ?, ?)";
    public static final String DELETE = "DELETE FROM USUARIO WHERE ID = ?";
    public static final String UPDATE = "UPDATE USUARIO SET NOMBRES = ?, APELLIDOS = ?, FECHANAC = ? WHERE ID = ?";
    public static final String QUERY_BY_ID = "select * from USUARIO where id=?";
    private Integer id;
    private String nombres;
    private String apellidos;
    private Date fechanac;

    public Usuario() {
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFechanac() {
        return fechanac;
    }

    public void setFechanac(Date fechanac) {
        this.fechanac = fechanac;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return nombres.toUpperCase() + " " + apellidos.toUpperCase();
    }
}
