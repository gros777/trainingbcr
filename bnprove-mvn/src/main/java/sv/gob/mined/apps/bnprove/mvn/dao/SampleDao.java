/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.dao;

import sv.gob.mined.apps.bnprove.mvn.fentities.Usuario;

/**
 *
 * @author xtecuan
 */
public interface SampleDao {

    public Usuario guardarUsuario(Usuario usuario);

    public Usuario encontrarPorRowId(String rowId);

    public Usuario encontrarPorId(Integer id);
}
