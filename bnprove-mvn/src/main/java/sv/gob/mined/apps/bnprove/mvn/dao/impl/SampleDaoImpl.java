/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.gob.mined.apps.bnprove.mvn.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import sv.gob.mined.apps.bnprove.mvn.dao.SampleDao;
import sv.gob.mined.apps.bnprove.mvn.fentities.Usuario;

/**
 *
 * @author xtecuan
 */
@Repository(value = "sampleDao")
public class SampleDaoImpl implements SampleDao {

    private static final Logger logger = Logger.getLogger(SampleDaoImpl.class);
    @Autowired
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Usuario> userMapper = new RowMapper<Usuario>() {

        @Override
        public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
            Usuario salida = new Usuario();

            salida.setNombres(rs.getString("nombres"));
            salida.setApellidos(rs.getString("apellidos"));
            salida.setId(rs.getInt("id"));
            salida.setFechanac(new java.util.Date(rs.getDate("fechanac").getTime()));

            return salida;
        }
    };

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public SampleDaoImpl() {
    }

    @Override
    public Usuario guardarUsuario(final Usuario usuario) {

        jdbcTemplate = new JdbcTemplate(dataSource);

        KeyHolder holder = new GeneratedKeyHolder();

        int r = jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(Usuario.INSERT, new String[]{"id"});

                ps.setString(1, usuario.getNombres());
                ps.setString(2, usuario.getApellidos());
                ps.setDate(3, new java.sql.Date(usuario.getFechanac().getTime()));

                return ps;
            }
        }, holder);

        Integer key = holder.getKey().intValue();

        usuario.setId(key);

        return usuario;
    }

    @Override
    public Usuario encontrarPorRowId(String rowId) {
        Usuario u = jdbcTemplate.queryForObject(Usuario.QUERY_BY_ROWID, new Object[]{rowId}, userMapper);
        return u;
    }

    @Override
    public Usuario encontrarPorId(Integer id) {
        Usuario u = jdbcTemplate.queryForObject(Usuario.QUERY_BY_ID, new Object[]{id}, userMapper);
        return u;
    }

}
