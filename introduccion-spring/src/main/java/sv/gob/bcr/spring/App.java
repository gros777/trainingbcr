package sv.gob.bcr.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 * 
 */
public class App {

	private Usuario usuario;

	public App() {

	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public static void main(String[] args) {
		//Instancio el contexto de Spring
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"hellospring-beans.xml");
		//Solicito un objeto pre construido del contexto
		App testApp = (App) context.getBean("app");
		//utilzo el objeto obtenido del contexto.
		System.out.println(testApp.getUsuario().toString());

	}
}
