package sv.gob.bcr.spring;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
public class Usuario{
	
	private String nombre;
	private String direccion;
	private String email;
	private Date birthdate;

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}


	public String toString(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String birthDateString = sdf.format(birthdate);
		String stringUsuario = "Nombre: " + this.nombre + "\n Direccion: " + this.direccion
			+ "\n e-mail: " + this.email + "\n Nacimiento: " + birthDateString;
		return stringUsuario;
	}

}
