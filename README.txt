##############################################
#    Training BCR 28/10/2013 - 29/11/2013    #
#    Impartido por:                          #
#         Ing. Julian Rivera (xtcuan)        #
#         Ing. Victor Hernandez(gros777)     #
##############################################


Este es un proyecto que contiene una serie de 
ejemplos relacionados a tecnolgías JAVA EE

Entre las tecnologías ejemplificadas se puede
encontrar:
	1. JSP
	2. Servlets
	3. EL
	4. JSTL
	5. JSF 2.0
	6. Spring 3.0
	7. Hibernate 4.0
	8. EJB 3.0
	9. Struts 2


